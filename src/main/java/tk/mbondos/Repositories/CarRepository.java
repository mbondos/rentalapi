package tk.mbondos.Repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tk.mbondos.Entities.Car;
@RepositoryRestResource(collectionResourceRel = "car", path = "cars")
public interface CarRepository extends JpaRepository<Car, Long> {

}
