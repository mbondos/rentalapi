package tk.mbondos.Utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {
    private static SessionFactory sessionFactory = buildSessionFactory();

    /*static {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(new Car("Ferrari", "LaFerrari", 20.99));
        session.save(new Car("Ferrari", "F40", 199.99));
        session.getTransaction().commit();

    }
*/
    private static SessionFactory buildSessionFactory() {
        try {
            SessionFactory session = new Configuration().configure().buildSessionFactory();
            return session;
        } catch (Throwable ex) {

            System.err.println("Initial SessionFactory creation failed" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
